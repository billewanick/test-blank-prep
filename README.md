# Exploring Matlab Alternatives

Make use of the `flake.nix` and `$ nix develop .` in the command-line to install Sage and Octave.

- Helpful Links
  - Sage
    - Homepage:<https://www.sagemath.org>
    - Nix packages: <https://search.nixos.org/packages?query=sage>
  - GNU Octave
    - Homepage: <https://octave.org/>
    - Nix packages: <https://search.nixos.org/packages?query=octave>
